# Global Political News Monitor #
### -- Subpage of daily visualization (v2)###


## Introduction ##

* This is a web app application visualizing news from thousands media. The data comes from [GDELT](http://gdeltproject.org/). It updates per 15 minutes.
* Version : 2.0
* [Live Demo](https://km-unite.dev.un.org/sites/km-unite.un.org/apps/GDELTvizDaily2/index.html#/) 

## Related Application ##

* [GDELTvizMonthly](https://bitbucket.org/oictviz/gdeltvizmonthly)
* [GDELTvizDaily](https://bitbucket.org/oictviz/gdeltvizdaily)
* [GDELTscraper-server](https://bitbucket.org/oictviz/gdeltscraper-server)


## Setting up ##
### Requirement ####
* Local / web server. For local, you can use [BabyWebServer (Windows)](http://www.pablosoftwaresolutions.com/html/baby_web_server.html) or [MAMP (MAC)](https://www.mamp.info/en/) when testing the website on your local desktop. Other ways, such as SimpleHTTPServer, cannot have your website connect to the qlik server
* Member of OICT Viz BitBucket group
* Browser that supports HTML5

### Setting Up ####
To deploy this app on your machine, download by click `Downloads` at the left menu or clone the repository on your local or web server : 
```
git clone https://lemonsong@bitbucket.org/oictviz/gdeltvizdaily2.git

```
Navigate to this URL :
```
<server>:<port>/GDELTvizDaily2/index.html
```

### Contact ###

* Creator : Yilin Wei (weiy@un.org)