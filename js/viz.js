/**
 * Unite Analytics Viz Template
 * @author Kania Azrina | azrina@un.org
 * Global Politics Monitor
 * @author Yilin Wei | weiy@un.org
 **/

/* Required for Qlik Sense integration with angular-based mashup */
define("client.services/grid-service", {});

/* Connect to a Qlik Server */
var config = {
  host: 'viz.dev.un.org', // window.location.hostname,
  prefix: "/visualization/",
  port: "", // window.location.port,
  isSecure: true // window.location.protocol === "https:"
};

require.config({
  baseUrl:
		(config.isSecure ? "https://" : "http://")
		+ config.host
		+ (config.port ? ":" + config.port : "")
		+ config.prefix + "resources"
});

require(["js/qlik"], function(qlik) {
  qlik.setOnError( function ( error ) {
		alert( error.message );
	} );
  var qlikApp; //qlik App

  function getQlikApp() {
    return qlik.openApp("b50b53d6-be03-4801-8efe-032aec058a0b", config) //app ID on Qlik server
  }

	/** MODULES **/
  var webApp = angular.module("webApp", ['ngRoute']);
	/** ROUTES **/
  webApp.config(function($routeProvider) {
    $routeProvider
			.when("/", {templateUrl: "part/home.html",controller: "PageCtrl"}) //section 1 controller
			.when("/section1", {templateUrl: "part/section1.html",controller: "S1Ctrl"}) //section 1 controller
            //.when("/section2", {templateUrl: "part/section2.html",controller: "S2Ctrl"}) //section 2 controller
    	    // else 404
			.otherwise({templateUrl: "part/404.html",controller: "PageCtrl"});
  });

	/** CONTROLLERS **/
  webApp.controller("PageCtrl", function() {
    //if (!qlikApp) {
    qlikApp = getQlikApp();
    //}
   
    qlikApp.getObject('conflict-map',chartsUN['conflict-map']); //chart43
    qlikApp.getObject('conflict-date',chartsUN['conflict-date']);//chart44
    qlikApp.getObject('conflict-table',chartsUN['conflict-table']);//chart44
    qlikApp.getObject('conflict-event',chartsUN['conflict-event']);//chart45
    qlikApp.getObject('conflict-article', chartsUN['conflict-article']);//chart46
  });

  webApp.controller("S1Ctrl", ['$scope', '$location', function($scope, $location) {
    //if (!qlikApp) {
    qlikApp = getQlikApp();
    //bookmark
    qlikApp.bookmark.apply('PJsg'); //GDELT dataset

     /* Current Selections Bar */
	qlikApp.getObject('CurrentSelections','CurrentSelections');
    /* Daily */
    /* Info */
    qlikApp.getObject('event-map',chartsUN['event-map']); //chart1
    qlikApp.getObject('update-date', chartsUN['update-date']); //chart2
    qlikApp.getObject('total-event', chartsUN['total-event']); //chart3
    qlikApp.getObject('total-article', chartsUN['total-article']); //chart4
    /* Filter */
    qlikApp.getObject('stability-impact-filter', chartsUN['stability-impact-filter']); //chart5
    qlikApp.getObject('event-category-bar', chartsUN['event-category-bar']); //chart6
    qlikApp.getObject('event-type-bar', chartsUN['event-type-bar']); //chart7
    qlikApp.getObject('event-country-filter', chartsUN['event-country-filter']); //chart47
    qlikApp.getObject('date-added-filter', chartsUN['date-added-filter']); //chart49
    /* Filter - actor1 */
    qlikApp.getObject('actor1-group', chartsUN['actor1-group']); //chart8
    qlikApp.getObject('actor1-name', chartsUN['actor1-name']); //chart9
    qlikApp.getObject('actor1-country', chartsUN['actor1-country']); //chart10
    qlikApp.getObject('actor1-type', chartsUN['actor1-type']); //chart11
    qlikApp.getObject('actor1-religion', chartsUN['actor1-religion']); //chart12
    qlikApp.getObject('actor1-ethnicity', chartsUN['actor1-ethnicity']); //chart13
    /* Filter - actor2 */
    qlikApp.getObject('actor2-group', chartsUN['actor2-group']); //chart14
    qlikApp.getObject('actor2-name', chartsUN['actor2-name']); //chart15
    qlikApp.getObject('actor2-country', chartsUN['actor2-country']); //chart16
    qlikApp.getObject('actor2-type', chartsUN['actor2-type']); //chart17
    qlikApp.getObject('actor2-religion', chartsUN['actor2-religion']); //chart18
    qlikApp.getObject('actor2-ethnicity', chartsUN['actor2-ethnicity']); //chart19
    /* Table */
    qlikApp.getObject('event-table', chartsUN['event-table']); //chart20
      
    //clear all selections button
    $("#clear_selection").click(function() {
      qlikApp.clearAll(); 
    });

  }]);


	/** Bootstraping angular app for app, must be done before Qlik Sense API is used **/
  angular.bootstrap(document.documentElement, ["webApp", "qlik-angular"]);
  qlik.setOnError(function(error) {
    $("#errmsg").html(error.message).parent().show();
  });

});
