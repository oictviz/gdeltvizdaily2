/* Variable declaration */
var app;
var qlikApp; //qlik objects
var width = screen.width; //screen width
var chartsUN = {
        /* Mapping between Qlik object ID and chart name */

        /* Daily */
        /* Info */
        'event-map': 'PHS', //chart1
        'update-date': 'dkQrJN', //chart2
        'total-event': 'mNshtL', //chart3
        'total-article': 'JPNrvcF', //chart4

        /* Filter */
        'stability-impact-filter': 'svxkKY', //chart5
        'event-category-bar': 'nZpmptx', //chart6
        'event-type-bar': 'TVGGpCy', //chart7
        'event-country-filter': 'SVpskWF', //chart47
        'date-added-filter': 'uDkP', //chart49
        /* Filter - actor1 */
        'actor1-group': 'sFxxn', //chart8
        'actor1-name': 'TDkJSdM', //chart9
        'actor1-country': 'xPLVCMn', //chart10
        'actor1-type': 'nhGNTj', //chart11
        'actor1-religion': 'bcPMMd', //chart12
        'actor1-ethnicity': 'pxkqqxZ', //chart13
        /* Filter - actor2 */
        'actor2-group': 'UNhSPp', //chart14
        'actor2-name': 'SHtvA', //chart15
        'actor2-country': 'qpSjC', //chart16
        'actor2-type': 'PUSyQ', //chart17
        'actor2-religion': 'ycdSGsa', //chart18
        'actor2-ethnicity': 'UGpJsa', //chart19

        /* Table */
        'event-table': 'nRdjk', //chart20

        /* Home page */
        'conflict-map': 'XmjUTG',//chart43
        'conflict-date': 'PcsAS',//chart44
        'conflict-table':'mEDAg', //chart48
        'conflict-event': 'szygqW',//chart45
        'conflict-article': 'MPXkQ'//chart46
    };

var chartMap = {
        /* Daily */
        /* Info */
        'event-map': 1, //chart1
        'update-date': 2, //chart2
        'total-event': 3, //chart3
        'total-article': 4, //chart4
        /* Filter */
        'stability-impact-slider': 5, //chart5
        'event-category-bar': 6, //chart6
        'event-type-bar': 7, //chart7
        'event-country-filter': 47, //chart47
        'date-added-filter':49,
        /* Filter - actor1 */
        'actor1-group': 8, //chart8
        'actor1-name': 9, //chart9
        'actor1-country': 10, //chart10
        'actor1-type': 11, //chart11
        'actor1-religion': 12, //chart12
        'actor1-ethnicity': 13, //chart13
        /* Filter - actor2 */
        'actor2-group': 14, //chart14
        'actor2-name': 15, //chart15
        'actor2-country': 16, //chart16
        'actor2-type': 17, //chart17
        'actor2-religion': 18, //chart18
        'actor2-ethnicity': 19, //chart19
        /* Table */
        'event-table': 20,

        /* Home page */
        'conflict-map': 43, //chart43
        'conflict-date': 44, //chart44
        'conflict-table':48, //chart48
        'conflict-event': 45, //chart45
        'conflict-article': 46, //chart46
 };

var orHeight = [];

function displayImage(chartName){
    var id = chartMap[chartName];
    var img = document.createElement("img");
    img.src = 'images/charts/chart'+id+'.png';
    img.style.width = "100%";
    img.style.height = "auto";
    img.onload = function() {
    chartDiv = document.getElementById(chartName);
    orHeight[chartName] = chartDiv.style.height;
    chartDiv.appendChild(img);
    var divHeight = this.clientHeight;
    chartDiv.style.height = divHeight+"px";
    }
}

function setHeight(chartName){
  chartDiv = document.getElementById(chartName);
  chartDiv.style.height = orHeight[chartName];
}

/* HIDE BUTTONS 
$('#meetingsButton').hide();
$('#agendaButton').hide();
$('#decisionsButton').hide();
$('#subsButton').hide();*/

var config = {
      host: "viz.dev.un.org",
      prefix:"/visualization/",
      isSecure: true
};

require.config({
  baseUrl: (config.isSecure ? "https://" : "http://")
  + config.host
  + (config.port ? ":"
  + config.port : "")
  + config.prefix
  + "resources"
});

require(["js/qlik"], function(qlik) {
  qlikApp = qlik;
  qlik.setOnError(function(error) {
    console.log(error.message);
  });

  /* Establish connection with Qlik Server */
  app = qlikApp.openApp("b50b53d6-be03-4801-8efe-032aec058a0b", config);
    
  /* Only getting charts needed on the first load */
  /* Daily */
  /* Info */
  displayObject('event-map', true);
  displayObject('update-date', true);
  displayObject('total-event',true);
  displayObject('total-article',true);
  /* Filter*/
  displayObject('event-category-bar',true);
  displayObject('event-type-bar',true);
  displayObject('event-country-filter',false);
  displayObject('stability-impact-filter',false);
  displayObject('date-added-filter',false); 
  /* Filter - actor1 */
  displayObject('actor1-group', false);
  displayObject('actor1-name', false);
  displayObject('actor1-country',false);
  displayObject('actor1-type',false);
  displayObject('actor1-religion',false);
  displayObject('actor1-ethnicity', false);
  /* Filter - actor2 */
  displayObject('actor2-group', false);
  displayObject('actor2-name', false);
  displayObject('actor2-country',false);
  displayObject('actor2-type',false);
  displayObject('actor2-religion',false);
  displayObject('actor2-ethnicity', false);
    
  /* Home page */
  displayObject('conflict-map', true);//chart43
  displayObject('conflict-date', true);//chart44
  displayObject('conflict-table',true);
  displayObject('conflict-event', true);//chart45
  displayObject('conflict-article', true);//chart46

  /* Clear all selection */
  $("#clearSelection").click(function() {
    app.clearAll();
  });
    
});
                
function hideElement(elementId) {
    /* Hide element after page loads */
  $('#' + elementId).hide();
}

function removeImg(chartName){
  var chartDiv = document.getElementById(chartName);
  console.log(chartDiv);
  chartDiv.removeChild(chartDiv.childNodes[0])
}

function displayObject(key,mobile) {
  /* Display qlik objects */
  /* If mobile == false, not responsive on small screen */
  if (mobile == false){
    if (width < 480) {
      app.getObject(key, chartsUN[key], {
        noInteraction: true
      });

    } else {
      app.getObject(key, chartsUN[key]);
    }
  } else {
    app.getObject(key, chartsUN[key]);
  }
}

function printObj(obj) {
  var newWindow = window.open(obj);
  newWindow.print();
}

